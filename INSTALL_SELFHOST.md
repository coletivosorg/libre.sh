### install docker and docker-compose a decent version please..

# add kernel parameter.. why?? i trust my debian.. :o

```
cat > /etc/sysctl.d/libresh.conf <<EOF
fs.aio-max-nr=1048576
vm.max_map_count=262144
vm.overcommit_memory=1
EOF
chmod 644 /etc/sysctl.d/libresh.conf
sysctl -p /etc/sysctl.d/libresh.conf

echo never > /sys/kernel/mm/transparent_hugepage/enabled
```
# define environment - what is this used for anyway??

```
cat > /etc/environment <<EOF
NAMECHEAP_URL="namecheap.com"
NAMECHEAP_API_USER="pierreo"
NAMECHEAP_API_KEY=
IP="curl -s http://icanhazip.com/"
FirstName="Pierre"
LastName="Ozoux"
Address=""
PostalCode=""
Country="Portugal"
Phone="+351.967184553"
EmailAddress="pierre@ozoux.net"
City="Lisbon"
CountryCode="PT"
BACKUP_DESTINATION=root@xxxxx:port
MAIL_USER=
MAIL_PASS=
MAIL_HOST=mail.indie.host
MAIL_PORT=587
EOF
```

# install Libre.sh

```
git clone https://framagit.org/c1000101/libre.sh.git /libre.sh &&\
```
create your own data structure... we have like:

```
├── instances
│   ├── libre.coletivos.org
│   │   └── ...
│   └── libre.sh
│   │   └── ...
├── scripts
└── storage
    ├── libre.coletivos.org
    │   └── ...
    └── libre.sh
        └── trash
```
and we made symlinks like:
```
sudo ln -s /live/instances/ /data/domains
sudo ln -sf /live/storage/libre.sh/trash/ /data/trash
```

```
cp /libre.sh/unit-files/* /etc/systemd/system && systemctl daemon-reload &&\
systemctl enable web-net.service &&\
systemctl start web-net.service &&\
```
if I add this to my profile.d why the hell i need the opt/bin  ??? 
```
ln -s ${LIBRESH_DIR}/utils/* /opt/bin/
ln -s /usr/local/bin/docker-compose /opt/bin/
```

# add /opt/bin path

```
cat > /etc/profile.d/libre.sh <<EOF
export PATH=$PATH:/live/instances/libre.sh/utils
EOF
chmod 644 /etc/profile.d/libre.sh
```
